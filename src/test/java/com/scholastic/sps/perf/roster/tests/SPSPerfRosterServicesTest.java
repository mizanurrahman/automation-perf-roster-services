package com.scholastic.sps.perf.roster.tests;

import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Test;

import com.jayway.restassured.builder.ResponseSpecBuilder;
import com.jayway.restassured.response.ExtractableResponse;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.ResponseSpecification;

public class SPSPerfRosterServicesTest extends BaseTest
{
	//private String title="MR";
	private String firstName="Don";
	private String lastName="Limon";
	private String email;
	private String password="passed1";
	private String schoolId="411463";
	private List<String> userType=new ArrayList<String>();
	private String teacherSPSID;
	private String childUserId1;
	private String childUserId2;
	public String userName;
	
	// Create Students Info
	private String s_firstname;
	private String s_lastname;
	private String s1_firstname;
	private String s1_lastname;
	private String d1_firstname;
	private String d1_lastname;
	private String d2_firstname;
	private String d2_lastname;
	private String s_clientProfileId="";
	private String s_grade="01";
	//private String s_grade1="02";
	private String s_regId="";
	private String s_clientId="TCOOL";
	private List<Object>students=null; 
	//End Points
	private static final String ENDPOINT_ROSTER_ADD_STUDENT="/spsroster/addStudent";
	private static final String ENDPOINT_ROSTER_GET_ALLSTUDENTS="/spsroster/allStudents";
	private static final String ENDPOINT_ROSTER_UPDATE_STUDENTS="/spsroster/updateStudents";
	private static final String ENDPOINT_ROSTER_DELETE_STUDENTS="/spsroster/removeStudents"; 
	
	@Test
		public void createTeacherGetTeacherAndAddRosterStudentGetRosterStudentFailureTest()
		{
			System.out.println("##############################################################################################");
			System.out.println("************ Light Teacher Registration, Get Teacher, Add Student and Get Student *************");
			System.out.println("***** Filure Test - when try to add student with first and last name that already exists  *****");
			System.out.println("###############################################################################################");
			
			// Light Teacher Registration
			ExtractableResponse<Response> createTeacherResponse=
									given()
											.log().all()
											.contentType("application/json")
											.body(createTeacherPayload()).
									when()
											.post("http://fs-iam-spsapifacade-perf1.scholastic-labs.io/sps-api-facade/spsuser/?clientId=SPS").
									then()
											.statusCode(201)
											.spec(createTeacherResponseValidator())
											.extract();		
			teacherSPSID=createTeacherResponse.path("spsId");
			userName=createTeacherResponse.path("email");	
			System.out.println("Teacher's SPSID is: " +teacherSPSID);
				
			// Get Teacher 
			/*ExtractableResponse<Response> getTeacherResponse=
									given()
											.pathParam("spsId",teacherSPSID).
									when()
											.post("http://fs-iam-spsapifacade-perf1.scholastic-labs.io/sps-api-facade/spsuser/?clientId=SPS").
									then()
											.statusCode(200)
											.extract();		
			System.out.println(getTeacherResponse.asString());*/
			
			// Add Students
			students=new ArrayList<Object>();
			s_firstname=generateRandomName();
			s_lastname=generateRandomName();
			
			ExtractableResponse<Response> addStudentResponse=
									given()
											.log().all()
											.contentType("application/json")
											.body(addStudentPayload()).
									when()
											.post(ENDPOINT_ROSTER_ADD_STUDENT).
									then()
											.statusCode(200)
											.spec(addStudentResponseValidator())
											.extract();	
			System.out.println("Added Student No.1: " +addStudentResponse.asString());
			
						
			// Add another Student with same first and last name
			students=new ArrayList<Object>();
			s1_firstname=s_firstname;
			s1_lastname=s_lastname;
			
			ExtractableResponse<Response> addStudentResponse2=
									given()
											.log().all()
											.contentType("application/json")
											.body(addStudent2Payload()).
									when()
											.post(ENDPOINT_ROSTER_ADD_STUDENT).
									then()
											.statusCode(200)
											.spec(addStudentResponse2Validator())
											.extract();	
			System.out.println("**********************************************************************************");
			System.out.println("Added Student No.2 with Same First and Last Name: " +addStudentResponse2.asString());
			System.out.println("**********************************************************************************");
	
	
			// Get Students
			ExtractableResponse<Response> getStudentResponse1=
									given()
											//.log().all()
											.contentType("application/json")
											.body(getStudentPayload()).
									when()
											.post(ENDPOINT_ROSTER_GET_ALLSTUDENTS).
									then()
											.statusCode(200)
											.spec(getStudentResponse1Validator())
											.extract();	
			System.out.println("***********************************************************************");
			System.out.println("Get All the Students for this Teacher: " +getStudentResponse1.asString());
			System.out.println("***********************************************************************");
			
		}
	
	 @Test
		 public void createTeacherGetTeacherAndAddRosterStudentGetRosterStudentTest() throws InterruptedException
		 {
			System.out.println("##############################################################################################");
			System.out.println("************ Light Teacher Registration, Get Teacher, Add Student and Get Student *************");
			System.out.println("###############################################################################################");
			
			// Light Teacher Registration
			ExtractableResponse<Response> createTeacherResponse=
									given()
											.log().all()
											.contentType("application/json")
											.body(createTeacherPayload()).
									when()
											.post("http://fs-iam-spsapifacade-perf1.scholastic-labs.io/sps-api-facade/spsuser/?clientId=SPS"). 
									then()
											.statusCode(201)
											.spec(createTeacherResponseValidator())
											.extract();		
			teacherSPSID=createTeacherResponse.path("spsId");
			userName=createTeacherResponse.path("email");	
			System.out.println("Teacher's SPSID is: " +teacherSPSID);
				
			// Get Teacher 
			/*ExtractableResponse<Response> getTeacherResponse=
									given()
											.pathParam("spsId",teacherSPSID).
									when()
											.get("http://fs-iam-spsapifacade-qa1.scholastic-labs.io/sps-api-facade/spsuser/{spsId}?clientId=SPSFacadeAPI").
									then()
											.statusCode(200)
											.extract();		
			System.out.println(getTeacherResponse.asString());*/
			
			// Add Students
			students=new ArrayList<Object>();			
			s_firstname=generateRandomName();
			s_lastname=generateRandomName();
			
			ExtractableResponse<Response> addStudentResponse=
									given()
											.log().all()
											.contentType("application/json")
											.body(addStudentPayload()).
									when()
											.post(ENDPOINT_ROSTER_ADD_STUDENT).
									then()
											.statusCode(200)
											.spec(addStudentResponseValidator())
											.extract();	
			System.out.println("Added Student No.1: " +addStudentResponse.asString());
			
			// Add another Student
			students=new ArrayList<Object>();	
			Thread.sleep(5000L);
			s_firstname=generateRandomName();
			s_lastname=generateRandomName();
			
			ExtractableResponse<Response> addStudentResponse1=
									given()
											.log().all()
											.contentType("application/json")
											.body(addStudent1Payload()).
									when()
											.post(ENDPOINT_ROSTER_ADD_STUDENT).
									then()
											.statusCode(200)
											.spec(addStudentResponse1Validator())
											.extract();	
			System.out.println("Added Student No.2: " +addStudentResponse1.asString());
			
			// Get Students
			ExtractableResponse<Response> getStudentResponse=
									given()
											
											.contentType("application/json")
											.body(getStudentPayload()).
									when()
											.post(ENDPOINT_ROSTER_GET_ALLSTUDENTS).
									then()
											.statusCode(200)
											.spec(getStudentResponseValidator())
											.extract();	
			System.out.println("Gets All the Students: " +getStudentResponse.asString());
		}
	 
	 @Test
		public void createTeacherGetTeacherAndAddRosterStudentEditStudentGetRosterStudentTest()throws InterruptedException
		{
			System.out.println("##############################################################################################");
			System.out.println("************ Light Teacher Registration, Get Teacher, Add Student, Edit and Get Student ******");
			System.out.println("###############################################################################################");
			
			// Light Teacher Registration
			ExtractableResponse<Response> createTeacherResponse=
									given()
											.log().all()
											.contentType("application/json")
											.body(createTeacherPayload()).
									when()
											.post("http://fs-iam-spsapifacade-perf1.scholastic-labs.io/sps-api-facade/spsuser/?clientId=SPS").
									then()
											.statusCode(201)
											.spec(createTeacherResponseValidator())
											.extract();		
			teacherSPSID=createTeacherResponse.path("spsId");
			userName=createTeacherResponse.path("email");	
			System.out.println("Teacher's SPSID is: " +teacherSPSID);
				
			// Get Teacher 
			/*ExtractableResponse<Response> getTeacherResponse=
									given()
											.pathParam("spsId",teacherSPSID).
									when()
											.post("http://fs-iam-spsapifacade-perf1.scholastic-labs.io/sps-api-facade/spsuser/?clientId=SPS").
									then()
											.statusCode(200)
											.extract();		
			System.out.println(getTeacherResponse.asString());*/
			
			// Add Students
			students=new ArrayList<Object>();			
			s_firstname=generateRandomName();
			s_lastname=generateRandomName();
			
			ExtractableResponse<Response> addStudentResponse=
									given()
											.log().all()
											.contentType("application/json")
											.body(addStudentPayload()).
									when()
											.post(ENDPOINT_ROSTER_ADD_STUDENT).
									then()
											.statusCode(200)
											.spec(addStudentResponseValidator())
											.extract();	
			System.out.println("Added Student No.1: " +addStudentResponse.asString());
			childUserId1=(addStudentResponse.path("students.childUserId")).toString();
			System.out.println("1st Student's User Id is: " +childUserId1);
			System.out.println("1st Student's First Name is: " +(addStudentResponse.path("students.firstName")));
			System.out.println("1st Student's Last Name is: " +(addStudentResponse.path("students.lastName")));
			
			// Add another Student
			students=new ArrayList<Object>();	
			Thread.sleep(5000L);
			s_firstname=generateRandomName();
			s_lastname=generateRandomName();
			
			ExtractableResponse<Response> addStudentResponse1=
									given()
											.log().all()
											.contentType("application/json")
											.body(addStudent1Payload()).
									when()
											.post(ENDPOINT_ROSTER_ADD_STUDENT).
									then()
											.statusCode(200)
											.spec(addStudentResponse1Validator())
											.extract();	
			System.out.println("Added Student No.2: " +addStudentResponse1.asString());
			childUserId2=(addStudentResponse1.path("students.childUserId")).toString();
			System.out.println("2nd Student's User Id is: " +childUserId2);
			System.out.println("2nd Student's First Name is: " +(addStudentResponse1.path("students.firstName")));
			System.out.println("2nd Student's Last Name is: " +(addStudentResponse1.path("students.lastName")));
			
			// Get students before update
			ExtractableResponse<Response> getStudentBeforeUpdateResponse=
					given()
							
							.contentType("application/json")
							.body(getStudentPayload()).
					when()
							.post(ENDPOINT_ROSTER_GET_ALLSTUDENTS).
					then()
							.statusCode(200)
							.spec(getStudentResponseValidator())
							.extract();	
			System.out.println("Gets the Students added before the Update: " +getStudentBeforeUpdateResponse.asString());
			
			
			//Update Student1
			students=new ArrayList<Object>();	
			Thread.sleep(5000L);
			s_firstname=generateRandomName();
			s_lastname=generateRandomName();
			ExtractableResponse<Response> updateStudentResponse1=
					given()
							.log().all()
							.contentType("application/json")
							.body(updateStudent1Payload()).
					when()
							.post(ENDPOINT_ROSTER_UPDATE_STUDENTS).
					then()
							.statusCode(200)
							.spec(updateStudentResponse1Validator())
							.extract();	
			System.out.println("Updated Students Info for the 1st Student: " +updateStudentResponse1.asString());
			System.out.println("1st Student's User Id is: " +(updateStudentResponse1.path("students.childUserId")).toString());
			System.out.println("Before the Update 1st Student's First Name was: " +(addStudentResponse.path("students.firstName")));
			System.out.println("Before the Update 1st Student's Last Name was: " +(addStudentResponse.path("students.lastName")));
			System.out.println("After the Update 1st Student's First Name is: " +(updateStudentResponse1.path("students.firstName")));
			System.out.println("After the Update 1st Student's Last Name is: " +(updateStudentResponse1.path("students.lastName")));
			
			//Update Student2
			students=new ArrayList<Object>();	
			Thread.sleep(5000L);
			s_firstname=generateRandomName();
			s_lastname=generateRandomName();
			ExtractableResponse<Response> updateStudentResponse2=
					given()
							.log().all()
							.contentType("application/json")
							.body(updateStudent2Payload()).
					when()
							.post(ENDPOINT_ROSTER_UPDATE_STUDENTS).
					then()
							.statusCode(200)
							.spec(updateStudentResponse2Validator())
							.extract();	
			System.out.println("Updated Students Info for the 1st Student: " +updateStudentResponse2.asString());
			System.out.println("2nd Student's User Id is: " +(updateStudentResponse2.path("students.childUserId")).toString());
			System.out.println("Before the Update 2nd Student's First Name was: " +(addStudentResponse1.path("students.firstName")));
			System.out.println("Before the Update 2nd Student's Last Name was: " +(addStudentResponse1.path("students.lastName")));
			System.out.println("After the Update 2nd Student's First Name is: " +(updateStudentResponse2.path("students.firstName")));
			System.out.println("After the Update 2nd Student's Last Name is: " +(updateStudentResponse2.path("students.lastName")));
			
			// Get students after update
			ExtractableResponse<Response> getStudentAfterUpdateResponse=
					given()
							
							.contentType("application/json")
							.body(getStudentPayload()).
					when()
							.post(ENDPOINT_ROSTER_GET_ALLSTUDENTS).
					then()
							.statusCode(200)
							.spec(getStudentResponseValidator())
							.extract();	

			System.out.println("Gets the Students After the Update: " +getStudentAfterUpdateResponse.asString());
		}
	 
	 @Test
		public void createTeacherDeleteNonExistingStudentsFromRosterTest()
		{
			System.out.println("##############################################################################################");
			System.out.println("************ Light Teacher Registration, Delete NonExisting Students From the Roster *********");
			System.out.println("###############################################################################################");
			
			// Light Teacher Registration
			ExtractableResponse<Response> createTeacherResponse=
									given()
											.log().all()
											.contentType("application/json")
											.body(createTeacherPayload()).
									when()
											.post("http://fs-iam-spsapifacade-perf1.scholastic-labs.io/sps-api-facade/spsuser/?clientId=SPS").
									then()
											.statusCode(201)
											.spec(createTeacherResponseValidator())
											.extract();		
			teacherSPSID=createTeacherResponse.path("spsId");
			userName=createTeacherResponse.path("email");	
			System.out.println("Teacher's SPSID is: " +teacherSPSID);
			
			// Delete students those do not exist.
			students=new ArrayList<Object>();
			childUserId1=generateRandomNumber();
			childUserId2=generateRandomNumber();
			ExtractableResponse<Response> deleteStudentResponse=
									given()
											.log().all()
											.contentType("application/json")
											.body(deleteStudentPayload()).
									when()
											.post(ENDPOINT_ROSTER_DELETE_STUDENTS).
									then()
											.statusCode(200)
											.spec(failureDeleteStudentResponseValidator())
											.extract();	
			System.out.println(deleteStudentResponse.asString());
			
		}
	 
	 @Test
		public void createTeacherGetTeacherAndAddRosterStudentDeleteStudentGetRosterStudentTest()throws InterruptedException
		{
			System.out.println("##############################################################################################");
			System.out.println("************ Light Teacher Registration, Get Teacher, Add Student, Delete and Get Student ******");
			System.out.println("###############################################################################################");
			
			// Light Teacher Registration
			ExtractableResponse<Response> createTeacherResponse=
									given()
											.log().all()
											.contentType("application/json")
											.body(createTeacherPayload()).
									when()
											.post("http://fs-iam-spsapifacade-perf1.scholastic-labs.io/sps-api-facade/spsuser/?clientId=SPS").
									then()
											.statusCode(201)
											.spec(createTeacherResponseValidator())
											.extract();		
			teacherSPSID=createTeacherResponse.path("spsId");
			userName=createTeacherResponse.path("email");	
			System.out.println("Teacher's SPSID is: " +teacherSPSID);
				
			// Get Teacher 
			/*ExtractableResponse<Response> getTeacherResponse=
									given()
											.pathParam("spsId",teacherSPSID).
									when()
											.post("http://fs-iam-spsapifacade-perf1.scholastic-labs.io/sps-api-facade/spsuser/?clientId=SPS").
									then()
											.statusCode(200)
											.extract();		
			System.out.println(getTeacherResponse.asString());*/
			
			// Add Students
			students=new ArrayList<Object>();			
			s_firstname=generateRandomName();
			s_lastname=generateRandomName();
			
			ExtractableResponse<Response> addStudentResponse=
									given()
											.log().all()
											.contentType("application/json")
											.body(addStudentPayload()).
									when()
											.post(ENDPOINT_ROSTER_ADD_STUDENT).
									then()
											.statusCode(200)
											.spec(addStudentResponseValidator())
											.extract();	
			System.out.println("Added Student No.1: " +addStudentResponse.asString());
			childUserId1=(addStudentResponse.path("students.childUserId")).toString();
			d1_firstname=(addStudentResponse.path("students.firstName"));
			d1_lastname=(addStudentResponse.path("students.lastName"));
			System.out.println("1st Student's User Id is: " +childUserId1);
			System.out.println("1st Student's First Name is: " +d1_firstname);
			System.out.println("1st Student's Last Name is: " +d1_lastname);
			
			// Add another Student
			students=new ArrayList<Object>();	
			Thread.sleep(5000L);
			s_firstname=generateRandomName();
			s_lastname=generateRandomName();
			
			ExtractableResponse<Response> addStudentResponse1=
									given()
											.log().all()
											.contentType("application/json")
											.body(addStudent1Payload()).
									when()
											.post(ENDPOINT_ROSTER_ADD_STUDENT).
									then()
											.statusCode(200)
											.spec(addStudentResponse1Validator())
											.extract();	
			System.out.println("Added Student No.2: " +addStudentResponse1.asString());
			childUserId2=(addStudentResponse1.path("students.childUserId")).toString();
			d2_firstname=(addStudentResponse1.path("students.firstName"));
			d2_lastname=(addStudentResponse1.path("students.lastName"));
			System.out.println("2nd Student's User Id is: " +childUserId2);
			System.out.println("2nd Student's First Name is: " +d2_firstname);
			System.out.println("2nd Student's Last Name is: " +d2_lastname);
			
			// Delete students
			students=new ArrayList<Object>();
			ExtractableResponse<Response> deleteStudentResponse=
									given()
											.log().all()
											.contentType("application/json")
											.body(deleteStudentPayload()).
									when()
											.post(ENDPOINT_ROSTER_DELETE_STUDENTS).
									then()
											.statusCode(200)
											.spec(deleteStudentResponseValidator())
											.extract();	
			System.out.println(deleteStudentResponse.asString());
			
		}
	 
	 	private String getEmail()
		{
			email="sps_perf"+String.valueOf(System.currentTimeMillis())+"@sampleTest.com";
			return email;
		}
		
		public String generateRandomName() 
		{
	      int length = 10;
	      boolean useLetters = true;
	      boolean useNumbers = false;
	      String generatedString = RandomStringUtils.random(length, useLetters, useNumbers);
	      return generatedString.toUpperCase();
		}
		
		public String generateRandomNumber() 
		{
	      int length = 8;
	      boolean useLetters = false;
	      boolean useNumbers = true;
	      String generatedNumbers = RandomStringUtils.random(length, useLetters, useNumbers);
	      return generatedNumbers;
		}
		
		private Map<String, Object> createTeacherPayload()
		
		{
			Map<String, Object> teacherRegistrationInfo = new HashMap<String, Object>();
			//teacherRegistrationInfo.put("title",title);
			teacherRegistrationInfo.put("firstName",firstName);
			teacherRegistrationInfo.put("lastName",lastName);
			teacherRegistrationInfo.put("email",getEmail());
			userType.add("TEACHER");
			teacherRegistrationInfo.put("userType", userType);
			teacherRegistrationInfo.put("password",password);
			teacherRegistrationInfo.put("userName",email);
			teacherRegistrationInfo.put("schoolId",schoolId);
			return teacherRegistrationInfo;
		}
		
		
		private Map<String, Object> addStudentPayload()
		
		{
			Map<String, Object> studentRegistrationInfo = new HashMap<String, Object>();
			//teacherRegistrationInfo.put("title",title);
			studentRegistrationInfo.put("firstName",s_firstname);
			studentRegistrationInfo.put("lastName",s_lastname);
			studentRegistrationInfo.put("grade",s_grade);
			studentRegistrationInfo.put("clientProfileId", s_clientProfileId);
			studentRegistrationInfo.put("regId",s_regId);
			students.add(studentRegistrationInfo);
			Map<String, Object> teacherAndAppInfo = new HashMap<String, Object>();
			teacherAndAppInfo.put("clientId",s_clientId);
			teacherAndAppInfo.put("teacherId",teacherSPSID);
			teacherAndAppInfo.put("students",students);
			System.out.println(teacherAndAppInfo.toString());
			return teacherAndAppInfo;
		}
		
		private Map<String, Object> addStudent1Payload()
		
		{
			Map<String, Object> studentRegistrationInfo = new HashMap<String, Object>();
			studentRegistrationInfo.put("firstName",s_firstname);
			studentRegistrationInfo.put("lastName",s_lastname);
			studentRegistrationInfo.put("grade",s_grade);
			studentRegistrationInfo.put("clientProfileId", s_clientProfileId);
			studentRegistrationInfo.put("regId",s_regId);
			students.add(studentRegistrationInfo);
			Map<String, Object> teacherAndAppInfo = new HashMap<String, Object>();
			teacherAndAppInfo.put("clientId",s_clientId);
			teacherAndAppInfo.put("teacherId",teacherSPSID);
			teacherAndAppInfo.put("students",students);
			System.out.println(teacherAndAppInfo.toString());
			return teacherAndAppInfo;
		}
		
		private Map<String, Object> addStudent2Payload()
		
		{
			Map<String, Object> studentRegistrationInfo = new HashMap<String, Object>();
			//teacherRegistrationInfo.put("title",title);
			studentRegistrationInfo.put("firstName",s1_firstname);
			studentRegistrationInfo.put("lastName",s1_lastname);
			studentRegistrationInfo.put("grade",s_grade);
			studentRegistrationInfo.put("clientProfileId", s_clientProfileId);
			studentRegistrationInfo.put("regId",s_regId);
			students.add(studentRegistrationInfo);
			Map<String, Object> teacherAndAppInfo = new HashMap<String, Object>();
			teacherAndAppInfo.put("clientId",s_clientId);
			teacherAndAppInfo.put("teacherId",teacherSPSID);
			teacherAndAppInfo.put("students",students);
			System.out.println(teacherAndAppInfo.toString());
			return teacherAndAppInfo;
		}
		
		private Map<String, Object> getStudentPayload()
		
		{
			Map<String, Object> clientTeaherInfo = new HashMap<String, Object>();
			clientTeaherInfo.put("clientId",s_clientId);
			clientTeaherInfo.put("teacherId",teacherSPSID);
			return clientTeaherInfo;
		}
		
		private Map<String, Object> updateStudent1Payload()
		
		{
			Map<String, Object> studentRegistrationInfo = new HashMap<String, Object>();
			studentRegistrationInfo.put("childUserId",childUserId1);
			studentRegistrationInfo.put("firstName",s_firstname);
			studentRegistrationInfo.put("lastName",s_lastname);
			studentRegistrationInfo.put("grade",s_grade);
			studentRegistrationInfo.put("clientProfileId", s_clientProfileId);
			studentRegistrationInfo.put("regId",s_regId);
			students.add(studentRegistrationInfo);
			Map<String, Object> teacherAndAppInfo = new HashMap<String, Object>();
			teacherAndAppInfo.put("clientId",s_clientId);
			teacherAndAppInfo.put("teacherId",teacherSPSID);
			teacherAndAppInfo.put("students",students);
			System.out.println(teacherAndAppInfo.toString());
			return teacherAndAppInfo;
		}
		
		private Map<String, Object> updateStudent2Payload()
		
		{
			Map<String, Object> studentRegistrationInfo = new HashMap<String, Object>();
			studentRegistrationInfo.put("childUserId",childUserId2);
			studentRegistrationInfo.put("firstName",s_firstname);
			studentRegistrationInfo.put("lastName",s_lastname);
			studentRegistrationInfo.put("grade",s_grade);
			studentRegistrationInfo.put("clientProfileId", s_clientProfileId);
			studentRegistrationInfo.put("regId",s_regId);
			students.add(studentRegistrationInfo);
			Map<String, Object> teacherAndAppInfo = new HashMap<String, Object>();
			teacherAndAppInfo.put("clientId",s_clientId);
			teacherAndAppInfo.put("teacherId",teacherSPSID);
			teacherAndAppInfo.put("students",students);
			System.out.println(teacherAndAppInfo.toString());
			return teacherAndAppInfo;
		}
		
		private Map<String, Object> deleteStudentPayload()
		
		{
			Map<String, Object> studentDeletionInfo = new HashMap<String, Object>();
			studentDeletionInfo.put("childUserId",childUserId1);
			Map<String, Object> studentDeletionInfo1 = new HashMap<String, Object>();
			studentDeletionInfo1.put("childUserId",childUserId2);
			students.add(studentDeletionInfo);
			students.add(studentDeletionInfo1);
			Map<String, Object> teacherAndAppInfo = new HashMap<String, Object>();
			teacherAndAppInfo.put("clientId",s_clientId);
			teacherAndAppInfo.put("teacherId",teacherSPSID);
			teacherAndAppInfo.put("students",students);
			System.out.println(teacherAndAppInfo.toString());
			return teacherAndAppInfo;
		}
		
		private ResponseSpecification createTeacherResponseValidator()
		
		{
			ResponseSpecBuilder rspec=new ResponseSpecBuilder()
			.expectBody("userName",equalTo(email))
			//.expectBody("modifiedDate",equalTo(""))
			//.expectBody("registrationDate",equalTo(new Date()))
			.expectBody("schoolId",equalTo(schoolId))
			.expectBody("orgZip",is(not(empty())))
			.expectBody("userType",equalTo(userType))
			.expectBody("isEducator",is(not(empty())))
			.expectBody("cac",is(not(empty())))
			.expectBody("cacId",is(not(empty())))
			.expectBody("isIdUsed",is(not(empty())))
			.expectBody("isEnabled",is(not(empty())))
			.expectBody("schoolUcn",is(not(empty())))
			.expectBody("email",equalTo(email))
			.expectBody("firstName",equalTo(firstName))
			.expectBody("lastName",equalTo(lastName))
			//.expectBody("password",equalTo(password))
			.expectBody("spsId",is(not(empty())));
			return rspec.build();		
		}
		
		private ResponseSpecification addStudentResponseValidator()
	
		{
			ResponseSpecBuilder rspec=new ResponseSpecBuilder()
			.rootPath("students")
			.expectBody("childUserId",is(not(empty())))
			.expectBody("clientProfileId",isEmptyString())
			.expectBody("errorCode",equalTo(200))
			.expectBody("errorMsg",equalTo("Operation success."))
			.expectBody("firstName",is(not(empty())))
			.expectBody("lastName",is(not(empty())))
			.expectBody("grade",equalTo("01"))
			.expectBody("regId",isEmptyString())
			.expectBody("status",equalTo("success"));
			return rspec.build();
		}
		
		private ResponseSpecification addStudentResponse1Validator()
	
		{
			ResponseSpecBuilder rspec=new ResponseSpecBuilder()
			.rootPath("students")
			.expectBody("childUserId",is(not(empty())))
			.expectBody("clientProfileId",isEmptyString())
			.expectBody("errorCode",equalTo(200))
			.expectBody("errorMsg",equalTo("Operation success."))
			.expectBody("firstName",is(not(empty())))
			.expectBody("lastName",is(not(empty())))
			.expectBody("grade",equalTo("01"))
			.expectBody("regId",isEmptyString())
			.expectBody("status",equalTo("success"));
			return rspec.build();
		}
		
		private ResponseSpecification getStudentResponseValidator()
	
		{
			ResponseSpecBuilder rspec=new ResponseSpecBuilder()
			.expectBody("code",equalTo(200))
			.expectBody("message",equalTo("Operation success."))
			.expectBody("status",equalTo("Successful"))
			.rootPath("students")
			.expectBody("childUserId[0]",is(not(empty())))
			.expectBody("clientProfileId[0]",isEmptyString())
			.expectBody("firstName[0]",is(not(empty())))
			.expectBody("lastName[0]",is(not(empty())))
			.expectBody("grade[0]",equalTo("01"))
			.expectBody("regId[0]",isEmptyString())
			.expectBody("status[0]",equalTo(true))
			
			.expectBody("childUserId[01]",is(not(empty())))
			.expectBody("clientProfileId[01]",isEmptyString())
			.expectBody("firstName[01]",is(not(empty())))
			.expectBody("lastName[01]",is(not(empty())))
			.expectBody("grade[01]",equalTo("01"))
			.expectBody("regId[01]",isEmptyString())
			.expectBody("status[01]",equalTo(true));
			return rspec.build();
		}
		
		private ResponseSpecification getStudentResponse1Validator()
	
		{
			ResponseSpecBuilder rspec=new ResponseSpecBuilder()
			.expectBody("code",equalTo(200))
			.expectBody("message",equalTo("Operation success."))
			.expectBody("status",equalTo("Successful"))
			.rootPath("students")
			.expectBody("childUserId",is(not(empty())))
			.expectBody("clientProfileId",isEmptyString())
			.expectBody("firstName",is(not(empty())))
			.expectBody("lastName",is(not(empty())))
			.expectBody("grade",equalTo("01"))
			.expectBody("regId",isEmptyString())
			.expectBody("status",equalTo(true));
			return rspec.build();
		}
		private ResponseSpecification addStudentResponse2Validator()
	
		{
			ResponseSpecBuilder rspec=new ResponseSpecBuilder()
			.rootPath("students")
			.expectBody("childUserId",is(not(empty())))
			.expectBody("clientProfileId",isEmptyString())
			.expectBody("errorCode",equalTo(282))
			.expectBody("errorMsg",equalTo("Student with this name already exists in roster."))
			.expectBody("firstName",is(not(empty())))
			.expectBody("lastName",is(not(empty())))
			.expectBody("grade",equalTo("01"))
			.expectBody("regId",isEmptyString())
			.expectBody("status",equalTo("failure"));
			return rspec.build();
		}
		private ResponseSpecification updateStudentResponse1Validator()
	
		{
			ResponseSpecBuilder rspec=new ResponseSpecBuilder()
			.rootPath("students")
			.expectBody("errorCode",equalTo(200))
			.expectBody("errorMsg",equalTo("Operation success."))
			.expectBody("childUserId",is(not(empty())))
			.expectBody("clientProfileId",isEmptyString())
			.expectBody("firstName",is(not(empty())))
			.expectBody("lastName",is(not(empty())))
			.expectBody("grade",equalTo("01"))
			.expectBody("regId",isEmptyString())
			.expectBody("status",equalTo("success"));
			return rspec.build();
		}
		
		private ResponseSpecification updateStudentResponse2Validator()
	
		{
			ResponseSpecBuilder rspec=new ResponseSpecBuilder()
			.rootPath("students")
			.expectBody("errorCode",equalTo(200))
			.expectBody("errorMsg",equalTo("Operation success."))
			.expectBody("childUserId",is(not(empty())))
			.expectBody("clientProfileId",isEmptyString())
			.expectBody("firstName",is(not(empty())))
			.expectBody("lastName",is(not(empty())))
			.expectBody("grade",equalTo("01"))
			.expectBody("regId",isEmptyString())
			.expectBody("status",equalTo("success"));
			return rspec.build();
		}
		
		private ResponseSpecification deleteStudentResponseValidator()
		
		{
			ResponseSpecBuilder rspec=new ResponseSpecBuilder()
			.rootPath("students")
			.expectBody("childUserId[0]",is(not(empty())))
			.expectBody("errorCode[0]",equalTo(200))
			.expectBody("errorMsg[0]",equalTo("Operation success."))
			.expectBody("status[0]",equalTo("success"))
			
			.expectBody("childUserId[01]",is(not(empty())))
			.expectBody("errorCode[01]",equalTo(200))
			.expectBody("errorMsg[01]",equalTo("Operation success."))
			.expectBody("status[01]",equalTo("success"));
			return rspec.build();
		}
		
		private ResponseSpecification failureDeleteStudentResponseValidator()
		
		{
			ResponseSpecBuilder rspec=new ResponseSpecBuilder()
			.rootPath("students")
			.expectBody("childUserId[0]",is(not(empty())))
			.expectBody("errorCode[0]",equalTo(284))
			.expectBody("errorMsg[0]",equalTo("Child SPS ID not found on roster."))
			.expectBody("status[0]",equalTo("failure"))
			
			.expectBody("childUserId[01]",is(not(empty())))
			.expectBody("errorCode[01]",equalTo(284))
			.expectBody("errorMsg[01]",equalTo("Child SPS ID not found on roster."))
			.expectBody("status[01]",equalTo("failure"));
			return rspec.build();
		}

}
